$(document).ready(function() {

	// Left Navigation transition
	$(window).scroll(function() {
		if ($(this).scrollTop() > 20) { //set some transition to better effect
			$('.leftNav').css({
				"top": "3%",
			    "transition-property": "top",
			    "transition-duration": "0.3s"
			});
		} else {
			$('.leftNav').css({
				"top": "15%",
			    "transition-property": "top",
			    "transition-duration": "0.3s"
			});
		}
	});


	//Right Navigation scrollTo
	 $('a[href*=#]:not([href=#])').click(function() {			//create active and read h2 from html
    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') 
        || location.hostname == this.hostname) {
        var target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
           if (target.length) {
             $('html,body').animate({
                 scrollTop: target.offset().top
            }, 1000);

            return false;
        	}
    	}
	});



});

