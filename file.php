<?php

class Wind101Types {

    const TEMPERATURE = 't,t1,t2';
    const HUMIDITY = 'h';
    const PRESSURE = 'p';
    const WINDSPEED = 'w';
    const WINDIRECTION = 'wd';
    const RAIN = 'r';
	const CLOUDCOVER = 'cloudcover';
	const FORECAST = 'forecast';
	const LOGBOOKSTART = 'LOGBOOKSTART';
	const ERROR = 'ERROR';
	const WARN = 'WARN';
	const INFO = 'INFO';
	const LOGBOOK = 'LOGBOOK';
	const DEBUG = 'DEBUG';
	const TRACE = 'TRACE';
	const PROPERTY = 'PROPERTY';
	const MAXWINDSPEED = 'max wind speed';
	const AVGWINDSPEED = 'avg wind speed';
	const AVGWINDDIRECTION = 'avg wind direction';
	const CNTR = 'cntr';
	
}

class Wind101API {

    const REQUEST_GET_DEVICES = 25;
    const REQUEST_GET_SENSORS = 29;
    const REQUEST_LOGIN = 3;
    const REQUEST_DATA = 15;
    
    private $token = 0;
    private $devices = array();
    private $unit_temperature = 'K';
    private $unit_humidity = 'float';
    
    
    public function login($email, $password) {
        $request = array(
            'c' => self::REQUEST_LOGIN,
            'e' => $email,
            'p' => $password);

        $response = $this->request($request);
        $this->token = $response["u"];

        return $this->token;
    }

    public function getDevices() {
        $request = array(
            'c' => self::REQUEST_GET_DEVICES,
            'u' => $this->token);

        $response = $this->request($request);
        $this->devices = $response['ai'];

		
        return $response['ai'];
    }

    public function getMostFreshDevice() {
        $ai = $this->getDevices();

        if (!$ai) {
            return null;
        }

        $maxRt = 0;
        $indexOfMaxRt = 0;
        foreach ($ai as $element => $item) {
            if ($item['rt'] > $maxRt) {
                $maxRt = $item['rt'];
                $indexOfMaxRt = $element;
            }
        }
        return $ai[$indexOfMaxRt];
    }
    

    
    public function getSensors($serialNumber) { 
        $request = array(
            'c' => self::REQUEST_GET_SENSORS,
            's' => $serialNumber,
            'u' => $this->token);

        $response = $this->request($request);


        return $response['ai'];
    }

    public function getSensorData($sensorId, $rt) {
        $request = array(
            'c' => self::REQUEST_DATA,
            'u' => $this->token,
            'ag' => 'LAST',
            'id' => $sensorId,
            'pe' => 432000,
            'rf' => 1410547633000,
            'rt' => $rt);
        $response = $this->request($request);
        //var_dump($request);
        return $response['ai'];
    }
    
    public function getSensorIdByType($serial, $type) {
        $allSensorsOfTheDevice = $this->getSensors($serial);        
        
        $temperatureArray = array();
        
        $types = explode(",", $type);
        
        foreach ($types as $type) {                       
            foreach ($allSensorsOfTheDevice as $sensor) {
                if (in_array($sensor['name'], $types)) {
                   return $sensor['id'];
               }
            }          
        }
        return null;
    }
    
    
    public function getData($serial, $array, $rt) { 
        $allSensors = array();
        
        foreach ($array as $type) {
            $sensorId = $this->getSensorIdByType($serial, $type);
            $temp = $this->getSensorData($sensorId, $rt); //sensors[0][rt]

            $value = $temp[0];
            switch ($type) {
                case Wind101Types::TEMPERATURE:
                    $temp[0]['value'] = $this->temperatureConvertor($value['value']);
                    break;
                case Wind101Types::HUMIDITY:
                    $temp[0]['value'] = $this->humidityConvertor($value['value']);
                    break;   
            }
            array_push($allSensors, array($temp[0]['value'], $temp[0]['ts'])); //[0], lebo inak mi to vytvori 1 zbytocne pole

        }
        return $allSensors;
        
    }
    
    
    public function temperatureConvertor($value) { 
        switch ($this->unit_temperature) {
            case 'K':
                $result = (float)$value;//primary unit
                break;
            case 'C':
                $result = (float)$value - 273.15;
                break;
            case 'F':
                $result = ((float)$value-273.15)*1.8+32;
        }
        return $result;
    }
    
    public function humidityConvertor($value) {
        switch ($this->unit_humidity) {
            case 'float':
                $result = (float)$value;//primary unit
                break;
            case '%':
                $result = (float)$value * 100;
        }
        return $result;
    }


    public function setUnitHumidity($unit) {
        static $allowed = array('float', '%');
        if (in_array($unit, $allowed)) {
            $this->unit_humidity = $unit;
        }
    }
    
    public function setUnitTemperature($unit) {
        static $allowed = array('K','C','F');
        if (in_array($unit, $allowed)) {
            $this->unit_temperature = $unit;
        }
        
    }
    
    private function request($array) {
        // create a new cURL resource
        $ch = curl_init();

        // set URL and other appropriate options
        curl_setopt($ch, CURLOPT_URL, "http://192.168.131.2:8099/"); //http://s.wind101.net:8099/
        curl_setopt($ch, CURLOPT_URL, "http://s.wind101.net:8099/");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($array));
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        // grab URL and pass it to the browser
        $response = curl_exec($ch);

        if (curl_errno($ch)) {
            echo 'Curl error: ' . curl_error($ch);
            //throw Exception instead of return
            return null;
        }

        // close cURL resource, and free up system resources
        curl_close($ch);

        return json_decode($response, true);
    }

}

$inst = new Wind101API();
$inst->login("info@wind101.net", "passwd");
$device = $inst->getMostFreshDevice();
//var_dump($device);


$devices = $inst->getDevices();
//var_dump($devices[1]);

$serial = $devices[4]['s']; //serial is Luxemburg
$sensors = $inst->getSensors($serial);
//var_dump($sensors);
//var_dump($sensors[0]['rt']);        //takto z�skam rt
//var_dump($sensors[0]['id']);          //sensor id

//$m = $inst->getSensorIdByType($devices[1], Wind101Types::TEMPERATURE); //17 m� vyhodi�
//var_dump($m['id']);


//$a = $inst->getSensorData(5994, time()*1000);
//var_dump($a);



$inst->setUnitHumidity('%');
$o = $inst->getData($serial, array(Wind101Types::HUMIDITY,Wind101Types::TEMPERATURE), time()*1000);
//var_dump($o);
?>

